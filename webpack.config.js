module.exports = {
  context: __dirname + "/src",
  entry: "./app.js",
  output: {
    path: __dirname + "/dist/build",
    publicPath: "/build/",
    filename: "bundle.js"
  },
  module: {
    loaders: [
      {test: /\.jsx?$/, exclude: /node_modules/, loaders: ["babel"]},
      {test: /\.scss$/, loaders: ["style", "css", "sass"]},
      {test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000'}
    ]
  },
  devServer: {
    port: 3000,
    inline: true,
    contentBase: 'dist',
    historyApiFallback: 'index.html',
    stats: {
      chunks: false,
    }
  }
};
