import React from 'react';
import {mount} from 'enzyme';
import {createStore} from 'redux';
import ContactForm from './contact-form';

import {defaultReducer} from '../../reducers';
const store = createStore(
  defaultReducer,
  {
    contacts: [{
      id: 1,
      name: 'hamid',
      email: '1@hello.in',
      phone: '123',
    }]
  }
);

function setup(userId=null) {
  const props = {
    params: {userId}
  };

  const enzymeWrapper = mount(
    <ContactForm {...props} />, {
      context: {store}
    }
  );

  return {
    props,
    enzymeWrapper
  }
}

describe('components', () => {
  describe('<ContactForm />', () => {
    it('should render edit contact form', () => {
      const { enzymeWrapper } = setup(1);
      expect(enzymeWrapper.find('form .btn').text()).toBe('Update');
      expect(enzymeWrapper.state('name')).toBe('hamid');
      expect(enzymeWrapper.state('email')).toBe('1@hello.in');
      expect(enzymeWrapper.state('phone')).toBe('123');
    });

    it('should render new contact form', () => {
      const { enzymeWrapper } = setup();
      expect(enzymeWrapper.find('form .btn').text()).toBe('Create');
      expect(enzymeWrapper.state('name')).toBe('');
      expect(enzymeWrapper.state('email')).toBe('');
      expect(enzymeWrapper.state('phone')).toBe('');
    });
  });
});

