import React, {Component} from 'react';
import { browserHistory } from 'react-router'
import uuid from 'node-uuid';

import ContactAction from '../../action/contact';

import './contact-form.scss';

export class ContactForm extends Component {

  state = {name: '', email: '', phone: ''}

  componentDidMount() {
    const id = this.props.params.userId || null;
    const {contacts} = this.context.store.getState();
    const contact = id ? contacts.filter(contact => contact.id == id)[0] : {name: ''};
    this.setState(contact || {name: ''});
  }

  handleInputChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit = e => {
    e.preventDefault();
    const {store} = this.context;
    if(this.state.id){
      store.dispatch(ContactAction.update(this.state.id, this.state));
    }else{
      store.dispatch(ContactAction.add({id: uuid.v4(), ...this.state}));
    }
    browserHistory.push('/contacts')
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit} className="new-contact">
        <div className="form-group">
          <div className="profile-image" style={{backgroundImage: this.state.name ? `url(http://api.adorable.io/avatars/300/${encodeURI(this.state.name)})`:''}}></div>
        </div>
        <div className="form-group">
          <input type="text" name="name" placeholder="Full Name" value={this.state.name} onChange={this.handleInputChange} required className="form-control" />
        </div>
        <div className="form-group">
          <input type="email" name="email" placeholder="Email" value={this.state.email} onChange={this.handleInputChange} required className="form-control" />
        </div>
        <div className="form-group">
          <input type="phone" name="phone" placeholder="Phone" value={this.state.phone} onChange={this.handleInputChange} required className="form-control" />
        </div>
        <div>
          <button className="btn btn-primary btn-block">{this.state.id ? 'Update':'Create'}</button>
        </div>
      </form>
    );
  }
}
ContactForm.contextTypes = {
  store: React.PropTypes.object
};

export default ContactForm;

