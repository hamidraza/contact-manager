import React, {Component} from 'react';

import './layout.scss';

export default class Layout extends Component {
  render() {
    return (
      <div className="main-layout container">
        {this.props.children}
        <footer className="main">
          Developed with React
        </footer>
      </div>
    );
  }
}
