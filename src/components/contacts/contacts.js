import React, {Component} from 'react';
import {Link} from 'react-router';

import ContactAction from '../../action/contact';

import Contact from '../contact';
import ContactForm from '../contact-form';

import './contacts.scss';

export class contacts extends Component {

  state = {contacts: [], search: ''}

  componentDidMount() {
    const {store} = this.context;
    const {contacts} = store.getState();
    this.setState({contacts});
    this.unsubscribe = store.subscribe(() => {
      const {contacts} = store.getState();
      this.setState({contacts});
    });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  handleDelete(id) {
    const {store} = this.context;
    store.dispatch(ContactAction.remove(id));
  }

  getContacts() {
    const search = this.state.search.toLocaleLowerCase();
    return this.state.search ? this.state.contacts.filter(contact => {
      const cn = contact.name.toLocaleLowerCase();
      const ce = contact.email? contact.email.toLocaleLowerCase() : '';
      const cp = contact.phone? contact.phone.toLocaleLowerCase() : '';
      return (cn.indexOf(search) != -1 || ce.indexOf(search) != -1 || cp.indexOf(search) != -1);
    }) : this.state.contacts;
  }

  handleInputChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  render() {
    const contacts = this.getContacts();

    return (
      <div>
        <div className="contacts-search">
          <div className="clearfix">
            <div className="pull-right submit-wrap">
              <Link type="submit" className="btn btn-primary tt-u" to="/contacts/new">
                <i className="glyphicon glyphicon-plus"></i> <span className="hidden-xs">&nbsp; Add contact</span>
              </Link>
            </div>
            <div className="ov-h input-wrap">
              <input type="text" className="form-control" placeholder="Search contacts" name="search" onChange={this.handleInputChange} value={this.state.search} />
            </div>
          </div>
          <p className="search-result">
            Showing <strong>{contacts.length}</strong> contact{contacts.length != 1?'s':''}
            {this.state.search?(
              <span>, of total {this.state.contacts.length} contact{this.state.contacts.length != 1?'s':''}</span>
            ):''}
          </p>
        </div>
        <div className="contacts row row-flex">
          {contacts.map(contact => (
            <div className="col-md-3 col-sm-4 col-xs-12" key={contact.id}>
              <Contact contact={contact} onDelete={this.handleDelete.bind(this)} />
            </div>
          ))}
          {!contacts.length? (
            <div className="no-contacts">
              No contacts
              {this.state.search? (
                <span>,  <a onClick={e => this.setState({search: ''})}>Clear search</a></span>
              ):''}
            </div>
          ):''}
        </div>
      </div>
    );
  }

}
contacts.contextTypes = {
  store: React.PropTypes.object
};

export default contacts;
