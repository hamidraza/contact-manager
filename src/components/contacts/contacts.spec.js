import React from 'react';
import {shallow, mount} from 'enzyme';
import sinon from 'sinon';
import {createStore} from 'redux';

import Contacts from './contacts';
import {defaultReducer} from '../../reducers';

const store = createStore(
  defaultReducer,
  {
    contacts: [{
      id: 1,
      name: 'hamid',
      email: '1@hello.in',
      phone: '123',
    }, {
      id: 2,
      name: 'raza',
      email: '2@hello.in',
      phone: '234',
    }]
  }
);

function setup() {
  const enzymeWrapper = mount(
    <Contacts />, {context:{store}}
  );

  return {
    enzymeWrapper
  }
}

describe('components', () => {
  describe('<Contacts />', () => {
    it('should render contacts', () => {
      sinon.spy(Contacts.prototype, 'componentDidMount');
      const {enzymeWrapper} = setup();

      expect(Contacts.prototype.componentDidMount.calledOnce).toBe(true);
      expect(enzymeWrapper.find('.search-result').text()).toBe('Showing 2 contacts');
      expect(enzymeWrapper.find('.contacts .contact-wrap').length).toBe(2);

      expect(
        enzymeWrapper.find('.contacts .contact-wrap').at(0).find('h2').text()
      ).toBe('hamid');

      expect(
        enzymeWrapper.find('.contacts .contact-wrap').at(1).find('.email a').text()
      ).toBe('2@hello.in');

      expect(
        enzymeWrapper.find('.contacts .contact-wrap').at(0).find('.phone a').text()
      ).toBe('123');
    });

    it('should not render contacts', () => {
      const {enzymeWrapper} = setup();
      enzymeWrapper.setState({ search: 'zzz123' });
      expect(enzymeWrapper.find('.search-result').text()).toBe('Showing 0 contacts, of total 2 contacts');
      expect(enzymeWrapper.find('.contacts .contact-wrap').length).toBe(0);
    });

    it('should render one contact', () => {
      const {enzymeWrapper} = setup();
      enzymeWrapper.setState({ search: '123' });
      expect(enzymeWrapper.find('.search-result').text()).toBe('Showing 1 contact, of total 2 contacts');
      expect(enzymeWrapper.find('.contacts .contact-wrap').length).toBe(1);
    });
  });
});
