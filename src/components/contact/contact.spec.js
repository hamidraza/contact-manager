import React from 'react';
import {shallow} from 'enzyme';
import Contact from './contact';

function setup() {
  const props = {
    contact: {
      id: 1,
      name: 'hamid',
      email: '1@hello.in',
      phone: '123',
    }
  };

  const enzymeWrapper = shallow(
    <Contact {...props} />
  );

  return {
    props,
    enzymeWrapper
  }
}

describe('components', () => {
  describe('<Contact />', () => {
    it('should render single contact block', () => {
      const { enzymeWrapper } = setup();
      expect(enzymeWrapper.find('.contact-wrap h2').text()).toBe('hamid');
      expect(enzymeWrapper.find('.email a').text()).toBe('1@hello.in');
      expect(enzymeWrapper.find('.phone a').text()).toBe('123');
    });
  });
});
