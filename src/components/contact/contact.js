import React, {Component} from 'react';
import {Link} from 'react-router';

import './contact.scss';

export class contact extends Component {
  render() {
    const {contact, onDelete=()=>{}} = this.props;

    return (
      <div className="contact-wrap">
        <div className="profile-image" style={{backgroundImage: `url(http://api.adorable.io/avatars/300/${encodeURI(contact.name)})`}}>
          <div className="delete-nav">
            <i className="glyphicon glyphicon-option-vertical"></i>
            <ul className="list-unstyled">
              <li onClick={e => onDelete(contact.id)}>Delete</li>
            </ul>
          </div>
          <Link className="edit" to={`/contact/${contact.id}`}><i className="glyphicon glyphicon-pencil"></i></Link>
        </div>
        <div className="content">
          <h2>{contact.name}</h2>
          <h3 className="email">
            <label>Email</label>
            <a href={`mailto:${contact.email}`}>{contact.email}</a>
          </h3>
          <h3 className="phone">
            <label>Phone/Mobile</label>
            <a href={`tel:${contact.phone}`}>{contact.phone}</a>
          </h3>
        </div>
      </div>
    );
  }
}

export default contact;

