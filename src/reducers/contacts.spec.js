import contactReducer from './contacts';

describe('contacts reducer', () => {
  it('should return the initial state', () => {
    expect(
      contactReducer(undefined, {})
    ).toEqual(
      []
    );
  });

  it('should handle ADD_CONTACT', () => {
    expect(
      contactReducer([], {
        type: 'ADD_CONTACT',
        contact: {name: 'hamid', id: 1}
      })
    ).toEqual([
      {name: 'hamid', id: 1}
    ])
  });

  it('should handle UPDATE_CONTACT', () => {
    expect(
      contactReducer([
        {id: 1, name: 'raza'}
      ], {
        type: 'UPDATE_CONTACT',
        contact: {name: 'hamid'},
        id: 1
      })
    ).toEqual([
      {id: 1, name: 'hamid'}
    ])
  });

  it('should handle REMOVE_CONTACT', () => {
    expect(
      contactReducer([
        {id: 1, name: 'raza'}
      ], {
        type: 'REMOVE_CONTACT',
        id: 1
      })
    ).toEqual([])
  });

});

