
export const contacts = (state = [], action) => {
  switch(action.type) {
    case 'ADD_CONTACT':
      return [...state, {...action.contact}]
    case 'REMOVE_CONTACT':
      return state.filter(contact => contact.id != action.id)
    case 'UPDATE_CONTACT':
      return state.map(contact =>
        contact.id == action.id ? {...contact, ...action.contact, id: action.id} : contact
      );
    default:
      return state;
  }
}

export default contacts;
