import { combineReducers } from 'redux';

import {contacts} from './contacts';

export const defaultReducer = combineReducers({
  contacts
});
