import React    from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, IndexRoute, IndexRedirect, Redirect, browserHistory} from 'react-router'
import {createStore} from 'redux';
import {Provider} from 'react-redux';

import Layout      from './components/layout/Layout.js';
import Contacts    from './components/contacts';
import ContactForm from './components/contact-form';
import NoMatch     from './components/no-match/NoMatch.js';

import {loadState, saveState} from './localStorage';
import {defaultReducer} from './reducers';

const persistedState = loadState();
const store = createStore(
  defaultReducer,
  persistedState
);

store.subscribe(() => {
  saveState({
    contacts: store.getState().contacts
  });
});

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={Layout}>
        <IndexRedirect to="contacts" />
        <Route path="contacts" component={Contacts} />
        <Redirect from="contact/new" to="contacts/new" />
        <Route path="contacts/new" component={ContactForm} />
        <Redirect from="contacts/:userId" to="contact/:userId" />
        <Route path="contact/:userId" component={ContactForm} />
      </Route>
      <Route path="*" component={NoMatch} />
    </Router>
  </Provider>,
  document.getElementById('app')
);

