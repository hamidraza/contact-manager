export const add = (contact = {}) => {
  return {
    type: 'ADD_CONTACT',
    contact
  }
}

export const remove = (id) => {
  return {
    type: 'REMOVE_CONTACT',
    id
  }
}

export const update = (id, contact = {}) => {
  return {
    type: 'UPDATE_CONTACT',
    id,
    contact
  }
}

export default {
  add,
  update,
  remove
}
