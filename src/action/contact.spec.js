import ContactActions from './contact';

// Testing contact actions
describe('actions', () => {

  it('should create an action to add a contact', () => {
    const contact = {name: 'hamid'};
    const expectedAction = {
      type: 'ADD_CONTACT',
      contact
    }
    expect(ContactActions.add(contact)).toEqual(expectedAction);
  });

  it('should create an action to delete a contact', () => {
    const id = "123";
    const expectedAction = {
      type: 'REMOVE_CONTACT',
      id
    }
    expect(ContactActions.remove(id)).toEqual(expectedAction);
  });

  it('should create an action to delete a contact', () => {
    const id = "123";
    const contact = {name: 'hamid'};
    const expectedAction = {
      type: 'UPDATE_CONTACT',
      id,
      contact
    }
    expect(ContactActions.update(id, contact)).toEqual(expectedAction);
  });

});
